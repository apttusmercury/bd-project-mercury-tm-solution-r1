<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notify_Collaboration_Request_Owner</fullName>
        <description>Notify Collaboration Request Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Apttus__ApttusEmailTemplates/Collaboration_Task_Assigned</template>
    </alerts>
    <rules>
        <fullName>Assign Collaboration RequestOwner</fullName>
        <actions>
            <name>Notify_Collaboration_Request_Owner</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>OR(AND(ISCHANGED(Apttus_Config2__Status__c),ISPICKVAL(Apttus_Config2__Status__c, &apos;Submitted&apos;)),AND(ISCHANGED(OwnerId),ISPICKVAL(Apttus_Config2__Status__c, &apos;Submitted&apos;)))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
