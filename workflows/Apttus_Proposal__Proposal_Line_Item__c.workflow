<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <rules>
        <fullName>APTS_Set Approval Status Proposal Line</fullName>
        <actions>
            <name>APTS_Set_Approval_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Evaluates the search filter and sets the value of the approval status to approval required.</description>
        <formula>AND
  (
    NOT(ISBLANK(TEXT(Apttus_QPConfig__Guidance__c))),
    NOT(ISPICKVAL(Apttus_QPConfig__Guidance__c, &quot;Green&quot;)),
    APTS_Item_Approved__c != true
  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
