<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Legal_Review_Notification</fullName>
        <description>Legal Review Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <recipients>
            <field>LastModifiedById</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Apttus__ApttusEmailTemplates/Legal_review_notification</template>
    </alerts>
    <rules>
        <fullName>Legal Review Email Notification</fullName>
        <actions>
            <name>Legal_Review_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email notification if the value exceeds $1m</description>
        <formula>(Quote_Grand_Total__c *CURRENCYRATE(text(CurrencyIsoCode)))&gt;1000000</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>create approved timestamp</fullName>
        <actions>
            <name>Update_TimeStamp_approved</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.Apttus_QPApprov__Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>create pending approval timestamp</fullName>
        <actions>
            <name>Update_TimeStamp_pending_approval</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus_Proposal__Proposal__c.Apttus_QPApprov__Approval_Status__c</field>
            <operation>equals</operation>
            <value>Pending Approval</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
