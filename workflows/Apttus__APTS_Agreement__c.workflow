<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contract_Terminated_or_Expired_Tender_Team</fullName>
        <description>Contract Terminated or Expired (Tender Team)</description>
        <protected>false</protected>
        <recipients>
            <recipient>Tender Specialist</recipient>
            <type>opportunityTeam</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Apttus__ApttusEmailTemplates/Agreement_Terminated_or_Expired</template>
    </alerts>
    <alerts>
        <fullName>Contract_terminated_CCO_Users</fullName>
        <description>Contract terminated (CCO Users)</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>BDX_CE_CCO</recipient>
            <type>role</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Apttus__ApttusEmailTemplates/Agreement_Terminated_or_Expired</template>
    </alerts>
    <rules>
        <fullName>Agreement Terminated or Expired notification</fullName>
        <actions>
            <name>Contract_Terminated_or_Expired_Tender_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Expired</value>
        </criteriaItems>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Terminated</value>
        </criteriaItems>
        <description>Agreement Terminated or Expired notification</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Terminate agreement email notification</fullName>
        <actions>
            <name>Contract_terminated_CCO_Users</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Apttus__APTS_Agreement__c.Apttus__Status__c</field>
            <operation>equals</operation>
            <value>Terminated</value>
        </criteriaItems>
        <description>to notify a CCO User when a Contract associated to an Order has expired.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
