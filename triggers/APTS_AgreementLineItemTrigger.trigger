trigger APTS_AgreementLineItemTrigger on Apttus__AgreementLineItem__c (after insert) {
    List<Apttus__AgreementLineItem__c> lstAli=new List<Apttus__AgreementLineItem__c>();
     for(Apttus__AgreementLineItem__c  ali : Trigger.new){
         
         Sub_Lot__c[] subLot=[select TenderOutcome__c from Sub_Lot__c where id = :ali.Sub_Lot__c];
          Lot__c[] lot=[select TenderOutcome__c from Lot__c where id = :ali.Lot__c];
         
         
         if((lot.size() > 0 && lot[0].TenderOutcome__c == 'Lost') || (subLot.size() > 0 && subLot[0].TenderOutcome__c == 'Lost')){
          
                lstAli.add(ali.clone(true));
              
         }
    }
    delete lstAli; 
}