trigger APTS_TaskTrigger on Task (Before insert) {
    
    
    for(Task task : Trigger.New){
        List<User> user = [Select BusinessUnit__c from User where id=:task.OwnerId];
        String BU = user[0].BusinessUnit__c;
        if(Trigger.isBefore){
            if(task.Subject.contains('Create dossier') && task.Type!= '' ){
                system.debug('True Dossier');
                task.Type = 'Dossier Creation';
                
                
            }else if((task.Subject.contains('Timelines and deadlines') || task.Subject.contains('Roles and responsibilities')) && task.Type!= '' ){
                task.Type = 'Project Plan';
            }
            else{if((task.Subject.contains('Identify BU(s)') || task.Subject.contains('Identify customer(s)')
                    ||task.Subject.contains('Download documentation') || task.Subject.contains('Book start up meeting')
                    ||task.Subject.contains('Create checklist of tasks') || task.Subject.contains('Feasibility assessment')
                    ||task.Subject.contains('Pre tender documentation')) && task.Type!= ''){
                	task.Type = 'To Do';
            }
                
            }
           task.BusinessUnit__c = BU; 
            task.RecordTypeId = '0125E000000L6g6QAC';
        }
    }
}