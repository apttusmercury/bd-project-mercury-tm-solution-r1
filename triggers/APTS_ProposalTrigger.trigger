trigger APTS_ProposalTrigger on Apttus_Proposal__Proposal__c (after insert,after delete) {
    List<Apttus_Proposal__Proposal__c> proposal = new List<Apttus_Proposal__Proposal__c>();
    List<Opportunity> oppFinalLst=new List<Opportunity>();
    String ProposalID;
    String Code;
    String accountCountry;
    if(!Trigger.isDelete){
        for(Apttus_Proposal__Proposal__c p : Trigger.New)
        {  
            if(Trigger.isInsert){
                List<Apttus_Proposal__Proposal__c> prop = [select ID,
                                                           Name,
                                                           Quote_Number__c,
                                                           Deal_Type__c,
                                                           DeliveryTerms__c,
                                                           Incoterms__c,
                                                           Opp_Record_Type__c,
                                                           Apttus_Proposal__Opportunity__c,
                                                           Apttus_Proposal__Account__r.BillingCountry from Apttus_Proposal__Proposal__c 
                                                           where id=: p.id];   
                
                List<Opportunity> lstOpp=[select ID,ProposalExists__c,(select ID from Apttus_Proposal__R00N70000001yUfDEAU__r) from Opportunity where ID=:p.Apttus_Proposal__Opportunity__c];
                List<BDCompanyInformation__c> bdCompany = [Select id,
                                                           Country__c,DeliveryTerms__c,InCoTerms__c 
                                                           from BDCompanyInformation__c ];
                
                List<Apttus_Config2__PriceList__c> priceList = [Select id,
                                                                Name,
                                                                Country__c 
                                                                from Apttus_Config2__PriceList__c];
                
                for(Apttus_Proposal__Proposal__c pp : prop)
                {	
                    accountCountry = pp.Apttus_Proposal__Account__r.BillingCountry;
                    ProposalID=(pp.Name).right(6);
                    
                    if(pp.Opp_Record_Type__c == 'Short Sales Cycle' && pp.Deal_Type__c == null)
                        Code='Q';
                    else if(pp.Opp_Record_Type__c == 'Opportunity' && (pp.Deal_Type__c == 'Standard BD Offer' || pp.Deal_Type__c == 'Advanced Offer'))
                        Code='Q';
                    else if(pp.Opp_Record_Type__c == 'Opportunity' && pp.Deal_Type__c == 'Request for Tender')
                        Code ='T';
                    else
                        Code='X';
                    
                    pp.Quote_Number__c=Code+ProposalID;  
                    
                    if(accountCountry != null){
                        for(BDCompanyInformation__c bd:bdCompany){
                            if(accountCountry.equals(string.valueOf(bd.Country__c))){  
                                System.debug('Entered Loop');
                                pp.BD_Company_Information__c = bd.Id;
                                pp.Incoterms__c = bd.InCoTerms__c;
                                pp.DeliveryTerms__c = bd.DeliveryTerms__c;
                                break;                    
                            }
                        }
                        
                        for(Apttus_Config2__PriceList__c pl:priceList){
                            if(accountCountry.equals(string.valueOf(pl.Country__c))){
                                pp.Apttus_QPConfig__PriceListId__c = pl.id;
                                break;
                            }
                        }
                    }
                    proposal.add(pp);
                }
                if(p.Apttus_Proposal__Opportunity__c != null){
                    for(Opportunity o:lstOpp)	
                    {
                        o.ProposalExists__c=true;
                        oppFinalLst.add(o);
                    }
                }
            }
        }
    }
    if(Trigger.isDelete){
        for(Apttus_Proposal__Proposal__c pOld : Trigger.old)
        {
            List<Opportunity> lstOppOld=[select ID,ProposalExists__c,(select ID from Apttus_Proposal__R00N70000001yUfDEAU__r) from Opportunity where ID=:pOld.Apttus_Proposal__Opportunity__c];
            
            for(Opportunity oOld :lstOppOld)
            {
                if((oOld.Apttus_Proposal__R00N70000001yUfDEAU__r).isEmpty())
                {
                    oOld.ProposalExists__c=false; 
                    oppFinalLst.add(oOld);
                }
            }
        }      
    }
    update proposal;
    update oppFinalLst;
}