trigger APTS_ProposalLineItemTrigger on Apttus_Proposal__Proposal_Line_Item__c (before insert, before update,after insert){
    
    List<Apttus_Proposal__Proposal_Line_Item__c> pList =new List<Apttus_Proposal__Proposal_Line_Item__c>();    
    for(Apttus_Proposal__Proposal_Line_Item__c  pli : Trigger.New){ 
        if(Trigger.isAfter)
        {
            Decimal price= pli.Apttus_QPConfig__NetPrice__c;
            if(price < 0)
            {
                pList.add(pli.clone(true));
            } 
            
        }
        if(Trigger.isBefore){ 
            
            List<Apttus_Proposal__Proposal__c> cCode =  [select Apttus_Proposal__Account__r.BillingCountry,Language__c  from Apttus_Proposal__Proposal__c where id=: pli.Apttus_Proposal__Proposal__c];
            String accountCountry = cCode[0].Apttus_Proposal__Account__r.BillingCountry;
            String language =cCode[0].Language__c;
            
            List<Product_Country__c> pCountries = [select Country__r.name,TaxRate__c,CatalogNumber__c from Product_Country__c where CatalogNumber__c = :pli.Apttus_Proposal__Product__c];
            List<ProductLanguageDesc__c> pLanguages = [select Language__r.name,ShortDescription__c from ProductLanguageDesc__c where CatalogNumber__c = :pli.Apttus_Proposal__Product__c];
            
            for(Product_Country__c p :pCountries){
                
                if(string.valueOf(p.Country__r.name) == accountCountry){
                    pli.VAT_Rate__c = p.TaxRate__c;                              
                }
            }
            
            for(ProductLanguageDesc__c p :pLanguages){
                if((language != 'English') && (string.valueOf(p.Language__r.name) == language)){        
                    pli.DescriptionInLocalLanguage__c = p.ShortDescription__c;                          
                }
            }     
        }
    }
    delete pList;
}