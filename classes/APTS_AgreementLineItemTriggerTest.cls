@isTest
public class APTS_AgreementLineItemTriggerTest {
    
    static testMethod void TestLostLot(){
        
        ID Rid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Temporary Account').getRecordTypeId();
        
        ID Rid1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Competitor').getRecordTypeId();
        TriggerFactory.bypassApex=true;
        
        Test.startTest();
        
        Account CA =new Account(Name='TestAccount1',RecordTypeId=Rid1,BillingCountry='Belgium',BillingStreet='abc',BillingCity='xyz',AccountCategory__c='Government',IsTopLevelAccount__c=true);
        insert CA;
        
        Account acc=new Account(Name='TestAccount2',RecordTypeId=Rid,BillingCountry='Belgium',BillingStreet='abc',BillingCity='xyz',AccountCategory__c='Government',IsTopLevelAccount__c=true);
        insert acc;
        
        Account accComp=new Account(ParentID=acc.ID,Name='TestAccount1',BillingCountry='Belgium',BillingStreet='abc',BillingCity='xyz',RecordTypeId=Rid);
        insert accComp;
        
        Opportunity opp = new Opportunity(name='TestOpp',AccountID=acc.Id,DealType__c='Standard BD Offer',CloseDate=System.today()+30,StageName='Identification',Competitor__c=CA.Id);
        insert opp;
    
        Apttus__APTS_Agreement__c agreement= new Apttus__APTS_Agreement__c(Name='Standard Agreement',Apttus__Account__c=acc.Id,Apttus__Related_Opportunity__c=opp.Id);
        insert agreement;
          
        Lot__c lot = new Lot__c(name='TestLot',TenderOutcome__c='Lost',Opportunity__c=opp.Id);
        insert lot;
        
        Sub_Lot__c slot = new Sub_Lot__c(name='TestSubLot',Lot__c=lot.ID,TenderOutcome__c='Lost');
        insert slot;
         
        Apttus__AgreementLineItem__c aLI = new Apttus__AgreementLineItem__c(Apttus__AgreementId__c=agreement.Id,Lot__c=lot.Id,Sub_Lot__c=slot.Id);
        insert aLI;
        
        Test.stopTest();
    }

}