public with sharing class TriggerFactory
{

    public static Boolean bypassApex {
        get {
            if (bypassApex == null) {
                List<User> currentUser = [select BypassApex__c from User where Id = :UserInfo.getUserId()];
                If (currentUser.size()>0)
                {
                    bypassApex = currentUser[0].BypassApex__c;
                }
                else
                {
                    bypassApex = true;
                }
            }
            return bypassApex;
        }
        set;
    }

    public static void createHandler(Schema.sObjectType soType) {
        ITrigger handler = getHandler(soType);
        
        if (handler == null) {
            throw new TriggerException ('No Trigger Handler registered for Object Type: ' + soType);
        }

        if (!bypassApex) {
            execute(handler);
        }
    }

    private static void execute(ITrigger handler) {
        if (Trigger.isBefore) {
            handler.bulkBefore();
            
            if (Trigger.isDelete) {
                for (SObject so : Trigger.old) {
                    handler.beforeDelete(so);
                }
            } else if (Trigger.isInsert) {
                for (SObject so : Trigger.new) {
                    handler.beforeInsert(so);
                }
            } else if (Trigger.isUpdate) {
                for (SObject so : Trigger.old) {
                    handler.beforeUpdate(so, Trigger.newMap.get(so.Id));
                }
            }
        } else {
            handler.bulkAfter();
            
            if (Trigger.isDelete) {
                for (SObject so : Trigger.old) {
                    handler.afterDelete(so);
                }
            } else if (Trigger.isInsert) {
                for (SObject so : Trigger.new) {
                    handler.afterInsert(so);
                }
            } else if (Trigger.isUpdate) {
                for (SObject so : Trigger.old) {
                    handler.afterUpdate(so, Trigger.newMap.get(so.Id));
                }
            }
        }
        
        handler.andFinally();
    }
    
    private static ITrigger getHandler(Schema.sObjectType soType) {
        String className = getClassName(soType);
        Type t = Type.forName(className);

        if (t != null) {
            return (ITrigger)t.newInstance();
        }

        System.debug(LoggingLevel.ERROR, 'Handler ' + className + 'cannot be found');
        
        return null;
    }

    private static String getClassName(Schema.sObjectType soType) {
        return soType.getDescribe().getLocalName().replace('__c', '') + 'Handler';
    }
}