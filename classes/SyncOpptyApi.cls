public without sharing class SyncOpptyApi {
    
    @InvocableMethod(label='Auto Sync Opportunity' description='It Sync Proposal Products to Opprotunity Procts when Quote is accepted')
    public static void syncOppty(List<ID> propID)
    { 
        Apttus_QPConfig.QPConfigWebService.syncWithOpportunity(propID.get(0),false);
      
    }
   
}