@isTest
public class APTS_ProposalTriggerTest {
    static testMethod void QuoteNumberTest()
    {
        ID Rid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Temporary Account').getRecordTypeId();
        
        ID Rid1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Competitor').getRecordTypeId();
        
        ID oppRecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity').getRecordTypeId();
        
        ID oppRecTypeSSC = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Short Sales Cycle').getRecordTypeId();
                
        TriggerFactory.bypassApex=true;
        
        Test.startTest();
        
        Account CA =new Account(Name='TestAccount1',RecordTypeId=Rid1,BillingCountry='Sweden',BillingStreet='abc',BillingCity='xyz',AccountCategory__c='Government',IsTopLevelAccount__c=true);
        insert CA;
        
        Account acc=new Account(Name='TestAccount2',RecordTypeId=Rid,BillingCountry='Sweden',BillingStreet='abc',BillingCity='xyz',AccountCategory__c='Government',IsTopLevelAccount__c=true);
        insert acc;
        
        Account accComp=new Account(ParentID=acc.ID,Name='TestAccount1',BillingCountry='Sweden',BillingStreet='abc',BillingCity='xyz',RecordTypeId=Rid);
        insert accComp;
        
        BDCompanyInformation__c bd = new BDCompanyInformation__c(Name='Bd Company Test',Country__c='Sweden');
        insert bd;
        
        Apttus_Config2__PriceList__c priceList = new Apttus_Config2__PriceList__c(Name='Test Pl',Country__c='Sweden');
        insert priceList;
        
        Opportunity opp = new Opportunity(RecordTypeId=oppRecType,name='TestOpp',AccountID=acc.Id,DealType__c='Standard BD Offer',CloseDate=System.today()+30,StageName='Identification',Competitor__c=CA.Id);
        insert opp;
        
        Opportunity opp2 = new Opportunity(name='TestOpp2',AccountID=acc.Id,DealType__c='Request for Tender',CloseDate=System.today()+30,StageName='Identification',Competitor__c=CA.Id);
        insert opp2;
        
        Opportunity oppSSC = new Opportunity(RecordTypeId=oppRecTypeSSC,name='TestOppSSC',AccountID=acc.Id,CloseDate=System.today()+30,StageName='Identification',Competitor__c=CA.Id);
        insert oppSSC;
        
        Apttus_Proposal__Proposal__c p = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Account__c=acc.ID,Apttus_Proposal__Opportunity__c=opp.Id);
		insert p;
        
        Apttus_Proposal__Proposal__c p1 = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Account__c=acc.ID,Apttus_Proposal__Opportunity__c=opp2.Id);
		insert p1;
        
        Apttus_Proposal__Proposal__c p2 = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Account__c=acc.ID,Apttus_Proposal__Opportunity__c=oppSSC.Id);
		insert p2;
        
        try{
        	delete p2;
        }catch(Exception e){}
        
        Test.stopTest();
    }
}