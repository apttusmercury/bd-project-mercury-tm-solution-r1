@isTest
public class APTS_UploadCSVControllerTest{
  
    	public static final String FLOW_NAME = 'ngFlow';
    
  		static testMethod void  testUploadCSVFunctionality() {
        ID Rid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Temporary Account').getRecordTypeId();        
        ID Rid1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Competitor').getRecordTypeId();        
        ID oppRecType = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Opportunity').getRecordTypeId();        
        ID oppRecTypeSSC = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Short Sales Cycle').getRecordTypeId();
      	
        TriggerFactory.bypassApex = true;
            
        Account CA =new Account(Name='TestAccount1',RecordTypeId=Rid1,BillingCountry='Belgium',BillingStreet='abc',BillingCity='xyz',AccountCategory__c='Government',IsTopLevelAccount__c=true);
      	insert CA;
        Account acc=new Account(Name='TestAccount2',RecordTypeId=Rid,BillingCountry='Belgium',BillingStreet='abc',BillingCity='xyz',AccountCategory__c='Government',IsTopLevelAccount__c=true);
      	insert acc; 
        Account accComp=new Account(ParentID=acc.ID,Name='TestAccount1',BillingCountry='Belgium',BillingStreet='abc',BillingCity='xyz',RecordTypeId=Rid);
        insert accComp;
        
        List<Opportunity> listOpportunity = new List<Opportunity>();
        Opportunity opp = new Opportunity(RecordTypeId=oppRecType,name='TestOpp',AccountID=acc.Id,DealType__c='Standard BD Offer',CloseDate=System.today()+30,StageName='Identification',Competitor__c=CA.Id);
        listOpportunity.add(opp);
        Opportunity opp2 = new Opportunity(name='TestOpp2',AccountID=acc.Id,DealType__c='Request for Tender',CloseDate=System.today()+30,StageName='Identification',Competitor__c=CA.Id);
        listOpportunity.add(opp2);
        Opportunity oppSSC = new Opportunity(RecordTypeId=oppRecTypeSSC,name='TestOppSSC',AccountID=acc.Id,CloseDate=System.today()+30,StageName='Identification',Competitor__c=CA.Id);
        listOpportunity.add(oppSSC);
        insert listOpportunity;
        
        Apttus_Config2__PriceList__c priceListSO01 = new Apttus_Config2__PriceList__c(Name='Test Pl',Country__c='Sweden');
        insert priceListSO01;
       	
        List<Product2> listProduct = new List<Product2>();            
        Product2 productSO01 = new Product2(Name='TestProduct',ProductCode='368609');
        Product2 productSO02 = new Product2(Name='TestProduct1',ProductCode='368650');
        listProduct.add(productSO01);
        listProduct.add(productSO02);
        insert listProduct; 
        
      	Lot__c lot = new Lot__c(name='Lot Test 1',TenderOutcome__c='Lost',Opportunity__c=opp.Id);
        insert lot;
        Sub_Lot__c slot = new Sub_Lot__c(name='Sub Lot Test 1.1',Lot__c=lot.ID,TenderOutcome__c='Lost');
        insert slot;
        Customer_Product_Description__c cpd = new Customer_Product_Description__c(Name='Test Prod 1',Sub_Lot__c=slot.ID);
        insert cpd;
            
        Apttus_Proposal__Proposal__c proposalSO = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Account__c=acc.ID,Apttus_Proposal__Opportunity__c=opp.Id,Language__c='Danish');
		insert proposalSO;
        Apttus_Config2__ProductConfiguration__c productConfigurationSO01 = new Apttus_Config2__ProductConfiguration__c(Name='Test ProductConfig',Apttus_QPConfig__Proposald__c=proposalSO.ID, Apttus_Config2__PriceListId__c = priceListSO01.ID);
        insert productConfigurationSO01;
            
        System.currentPageReference().getParameters().put('id', productConfigurationSO01.id);
        System.currentPageReference().getParameters().put('businessObjectId', proposalSO.id);
        System.currentPageReference().getParameters().put('flow', 'ngFlow');
        
        String csvFileLine1 = productSO01.ProductCode+',X10'+',Lot Test 1'+',Sub Lot Test 1.1'+',Test Prod 1'+'\n';
        String csvFileLine2 = productSO02.ProductCode+',2'+',Lot Test 1'+',Sub Lot Test 1.1'+',Test Prod 1'+'\n';
      	String csvFileLine3 = productSO02.ProductCode+',X'+',Lot Test 1'+',Sub Lot Test 1.1'+',Test Prod 1'+'\n';
      	String csvFileLine4 = productSO02.ProductCode+',2'+',Lot Test'+',Sub Lot Test 1.1'+',Test Prod 1'+'\n';
      	String csvFileLine5 = productSO02.ProductCode+'12,2'+',Lot Test 1'+',Sub Lot Test 1.1'+',Test Prod 1'+'\n';
      	String csvFileLine6 = productSO02.ProductCode+',2'+',Lot Test 1'+',Sub Lot Test 1'+',Test Prod 1'+'\n';
      	String csvFileLine7 = productSO02.ProductCode+',2'+',Lot Test 1'+',Sub Lot Test 1.1'+',Test Prod '+'\n';
        String csvFileLine8 = productSO02.ProductCode+',2'+', '+', '+', '+'\n';
      	String csvFileLine9 = productSO02.ProductCode+',X2'+', '+', '+', '+'\n';
      	String csvFileLine10 = productSO02.ProductCode+'1,2'+', '+', '+', '+'\n';
        Test.startTest();
        
        APTS_UploadCSVController uploadCSVControllerSO = new APTS_UploadCSVController();
        uploadCSVControllerSO.validateCSVFile();
        System.assert(uploadCSVControllerSO.displayValidProducts == false, 'Valid Products displayed for blank file');
        System.assert(uploadCSVControllerSO.displayInvalidProducts == false, 'Invalid Products displayed for blank file');

    	uploadCSVControllerSO.csvFileBody = Blob.valueOf('abcd');
        uploadCSVControllerSO.validateCSVFile();
        System.assert(uploadCSVControllerSO.displayValidProducts == false, 'Valid Products displayed for invalid file');
        System.assert(uploadCSVControllerSO.displayInvalidProducts == false, 'Invalid Products displayed for invalid file');

    	uploadCSVControllerSO.csvFileBody = Blob.valueOf(csvFileLine1);
        uploadCSVControllerSO.validateCSVFile();
        System.assert(uploadCSVControllerSO.displayValidProducts == false, 'Valid Products displayed for invalid ProductCode');
        System.assert(uploadCSVControllerSO.displayNoProducts == true, 'Invalid Products not displayed for invalid ProductCode');


        uploadCSVControllerSO.csvFileBody = Blob.valueOf(csvFileLine2);
        uploadCSVControllerSO.validateCSVFile();
        System.assert(uploadCSVControllerSO.displayValidProducts == false, 'Valid Products displayed for invalid quantity');
        System.assert(uploadCSVControllerSO.displayNoProducts == true, 'Invalid Products not displayed for invalid quantity');

        uploadCSVControllerSO.csvFileBody = Blob.valueOf(csvFileLine1+csvFileLine2+csvFileLine3+csvFileLine4+csvFileLine5+csvFileLine6+csvFileLine7+csvFileLine8+csvFileLine9+csvFileLine10);
        uploadCSVControllerSO.validateCSVFile();
        System.assert(uploadCSVControllerSO.displayValidProducts == true, 'Valid Products displayed for invalid quantity or ProductCode');
        System.assert(uploadCSVControllerSO.displayInvalidProducts == true, 'Invalid Products not displayed for invalid quantity ProductCode');

    	uploadCSVControllerSO.revalidateInvalidProduct();
        System.assert(uploadCSVControllerSO.displayValidProducts == true, 'Valid Products displayed for invalid quantity or ProductCode after Revalidating');
        System.assert(uploadCSVControllerSO.displayInvalidProducts == true, 'Invalid Products not displayed for invalid quantity ProductCode after Revalidating');

        for(APTS_UploadCSVController.ProductWrapperClass invalidProduct : uploadCSVControllerSO.listInvalidProducts){
      	invalidProduct.productCode = productSO01.ProductCode;
      	invalidProduct.quantity = 10;
        invalidProduct.lot = lot.name;
        invalidProduct.subLot = slot.name;
        invalidProduct.custProd = cpd.name;
            
    }
        uploadCSVControllerSO.revalidateInvalidProduct();        
        System.assert(uploadCSVControllerSO.displayValidProducts == true, 'Valid Products not displayed after Revalidating');
        System.assert(uploadCSVControllerSO.displayInvalidProducts == false, 'Invalid Products displayed after Revalidating');

        APTS_UploadCSVController.addMultiProductsVF(productConfigurationSO01.id, uploadCSVControllerSO.JSONlistValidProducts, '1','2' );
        APTS_UploadCSVController.addMultiProductsVF(productConfigurationSO01.id, uploadCSVControllerSO.JSONlistValidProducts, '10','20' );

        for(Integer i=0; i<1001; i++){
            csvFileLine1 = csvFileLine1+productSO01.ProductCode + ',2'+',Lot Test 1'+',Sub Lot Test 1.1'+',Test Prod 1'+',\n';
        }
        uploadCSVControllerSO.csvFileBody = Blob.valueOf(csvFileLine1);
        uploadCSVControllerSO.validateCSVFile();
        System.assert(uploadCSVControllerSO.displayValidProducts == false, 'Valid Products displayed for products more than 1000');
        System.assert(uploadCSVControllerSO.displayInvalidProducts == false, 'Invalid Products displayed for products more than 1000');
		uploadCSVControllerSO.cartId = productConfigurationSO01.id;
        PageReference pageReferenceSO = uploadCSVControllerSO.backToCart();
      	uploadCSVControllerSO.cartId = null;
        PageReference pageReferenceSO1 = uploadCSVControllerSO.backToCart();
        
        Test.stopTest();
            
        System.assert(pageReferenceSO1 == null, 'Return link expected : null ###### Actual :' +pageReferenceSO);

    }
}