public class APTS_UploadCSVController {
    public String cartId {get; set;}
    public String proposalId {get; set;}
    public String flow {get; set;}
    public Blob csvFileBody{get;set;}
    public string csvAsString{get;set;}
    private list<ProductWrapperClass> listProductsToAdd {get;set;}
    public list<ProductWrapperClass> listInvalidProducts {get;set;}
    public list<ProductWrapperClass> listValidProducts {get;set;}
    public String JSONlistValidProducts { get { return JSON.serialize(listValidProducts) ; } }
    public boolean displayValidProducts { get { return (listValidProducts != null && !listValidProducts.isEmpty() ) ; } }
    public boolean displayInvalidProducts { get { return (listInvalidProducts != null && !listInvalidProducts.isEmpty() ) ; } }
    public boolean displayNoProducts{get;set;}
    
    public APTS_UploadCSVController(){
        
        cartId      = Apexpages.currentpage().getparameters().get('id');
        proposalId  = Apexpages.currentpage().getparameters().get('businessObjectId');
        flow        = Apexpages.currentpage().getparameters().get('flow');
        csvFileBody = Blob.valueOf('');
        csvAsString = '';
        listProductsToAdd   = new list<ProductWrapperClass>();
        listInvalidProducts = new list<ProductWrapperClass>();
        listValidProducts   = new list<ProductWrapperClass>();
    }
    
    public void validateCSVFile(){
        
        listProductsToAdd.clear();
        listInvalidProducts.clear();
        listValidProducts.clear();
        if(csvFileBody == null || csvFileBody.toString()==''){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Please Select a File.'));
            return;
        }
        
        String[] csvFileLines = csvFileBody.toString().split('\n');

        Set<String> setProductCode = new Set<String>();
        Set<String> setLot = new Set<String>();
        Set<String> setSubLot = new Set<String>();
        Set<String> setCustProd = new Set<String>();
        if(csvFileLines.size() >= 1000 ){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'You are trying to add 100 or more lines.'));
            return;
        }
        for(Integer i=1; i < csvFileLines.size(); i++){
            try{
                string[] csvRecordData = csvFileLines[i].split(','); 
                setProductCode.add(csvRecordData[0]);
				setLot.add(csvRecordData[2]);
                setSubLot.add(csvRecordData[3].trim());
                setCustProd.add(csvRecordData[4].trim());
                ProductWrapperClass pw = new ProductWrapperClass(i, csvRecordData[0], csvRecordData[1].trim(),csvRecordData[2].trim(),csvRecordData[3].trim(),csvRecordData[4].trim());
                listProductsToAdd.add(pw);
            }
            
            catch(Exception exceptionSO){
                listProductsToAdd.clear();
                ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Invalid data at line ' + i + '. Please check the file'));
                System.debug('Exception Occurred :'+ exceptionSO);
                return;
            }
        }
        
        if(listProductsToAdd.size() == 0){
            displayNoProducts =true;
        }
        
        Map<String, Product2> mapProduct = new Map<String,Product2>();        
        for(Product2 productSO : [Select ID
                                            , Name
                                            , ProductCode
                                            , Description
                                            from Product2
                                            Where ProductCode in :setProductCode]){
            mapProduct.put(productSO.ProductCode,productSO);
        }
         
        Map<String,Lot__c> mapLot = new Map<String,Lot__c>();
		Map<String,Sub_Lot__c> mapSubLot = new Map<String,Sub_Lot__c>();  
        Map<String,Customer_Product_Description__c> mapCustProd = new Map<String,Customer_Product_Description__c>(); 
        List<Apttus_Proposal__Proposal__c> oppId= [Select Apttus_Proposal__Opportunity__c                                            
                                      from Apttus_Proposal__Proposal__c
                                      Where id =:proposalId];
        
        String oppID1 = oppId[0].Apttus_Proposal__Opportunity__c;
        for(Lot__c lot : [Select ID , Name                                           
                                      from Lot__c
                                      Where name in :setLot and Opportunity__c =:oppID1]){
             for(Sub_Lot__c subLot : [Select ID , Name                                           
                                      from Sub_Lot__c
                                      Where name in :setSubLot and Lot__c=: lot.id]){
              for(Customer_Product_Description__c custProd : [Select ID , Name                                           
                                      								from Customer_Product_Description__c
                                      								Where name in :setCustProd and Sub_Lot__c=: subLot.id]){
           			mapCustProd.put(custProd.name,custProd);
        		}
                             
            	mapSubLot.put(subLot.name,subLot);
        	}                             
                                          
            mapLot.put(lot.name,lot);
        }
        
        for(ProductWrapperClass productToAdd : listProductsToAdd){
            if(productToAdd.lot != ''){
                if((mapProduct.containsKey(productToAdd.productCode))
               		&&(mapLot.containsKey(productToAdd.lot))
                		&&(mapSubLot.containsKey(productToAdd.subLot))
                			&&(mapCustProd.containsKey(productToAdd.custProd))){
                   Product2 productSO = mapProduct.get(productToAdd.productCode);
                Lot__c lot1 = mapLot.get(productToAdd.lot);
                Sub_Lot__c subLot1 = mapSubLot.get(productToAdd.subLot);
                Customer_Product_Description__c cProduct = mapCustProd.get(productToAdd.custProd);
                
                productToAdd.productId = productSO.ID;
                productToAdd.name = productSO.name;
                productToAdd.lotID  =  lot1.ID;
                productToAdd.subLotID  =  subLot1.ID;
                productToAdd.custProdID = cProduct.ID;
                
                if(productToAdd.quantity > 0){
                    listValidProducts.add(productToAdd);
                }
                else{
                    productToAdd.error = 'Invalid Quantity';
                    listInvalidProducts.add(productToAdd);
                }                
            }
            
            else{
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Product Code : ' + productToAdd.productCode + '.'));
                
                if(!(mapProduct.containsKey(productToAdd.productCode))){
                     productToAdd.error = 'Invalid Product Code';
                	 listInvalidProducts.add(productToAdd);
                }else if(!(mapLot.containsKey(productToAdd.lot))){
                    productToAdd.error = 'Please Check Lot Name';
                    listInvalidProducts.add(productToAdd);
                }else if(!(mapSubLot.containsKey(productToAdd.subLot))){
                    productToAdd.error = 'Please Check Sub Lot Name';
                    listInvalidProducts.add(productToAdd);
                }else{
                    productToAdd.error = 'Please Check Customer Ref# ';
                    listInvalidProducts.add(productToAdd);
                }
                
            }
            }
            else{
                if(mapProduct.containsKey(productToAdd.productCode)){
             Product2 productSO = mapProduct.get(productToAdd.productCode);
                productToAdd.productId = productSO.ID;
                productToAdd.name = productSO.name;
                
                if(productToAdd.quantity > 0){
                    listValidProducts.add(productToAdd);
                }
                else{
                    productToAdd.error = 'Invalid Quantity';
                    listInvalidProducts.add(productToAdd);
                }                
            }
            
            else{
                //ApexPages.addMessage(new ApexPages.Message(ApexPages.severity.ERROR,'Invalid Product Code : ' + productToAdd.productCode + '.'));
                productToAdd.error = 'Invalid Product Code';
                listInvalidProducts.add(productToAdd);
            }
            }
            
        }
        listProductsToAdd.clear();}
        
    
    
    public PageReference backToCart(){  
        system.debug('Inside backtocart'+cartID);
        if(APTS_UploadCSVController.saveCart(cartID)){
            PageReference pageReferenceSO = Page.Apttus_QPConfig__ProposalConfiguration;
            pageReferenceSO.getParameters().put('id', proposalId);
            pageReferenceSO.getParameters().put('flow', flow);
            return pageReferenceSO;
        }
        else{
            return null;
        }
    }
    
    public void revalidateInvalidProduct(){
        
        Set<String> setProductCode = new Set<String>();
        Set<String> setLot = new Set<String>();
        Set<String> setSubLot = new Set<String>();
        Set<String> setCustProd = new Set<String>();
        
        for(ProductWrapperClass invalidProduct : listInvalidProducts){
            //if(invalidProduct.isSelected){
            setProductCode.add(invalidProduct.productCode);
            setLot.add(invalidProduct.lot);
            setSubLot.add(invalidProduct.subLot);
            setCustProd.add(invalidProduct.custProd);
            //}
        }
        
        Map<String, Product2> mapProduct = new Map<String,Product2>();        
        for(Product2 productSO : [Select ID
                                            , Name
                                            , ProductCode
                                            , Description
                                            from Product2
                                            Where ProductCode in :setProductCode]){
            mapProduct.put(productSO.ProductCode,productSO);
        }
        
        Map<String,Lot__c> mapLot = new Map<String,Lot__c>();
		Map<String,Sub_Lot__c> mapSubLot = new Map<String,Sub_Lot__c>();  
        Map<String,Customer_Product_Description__c> mapCustProd = new Map<String,Customer_Product_Description__c>();
		
        List<Apttus_Proposal__Proposal__c> oppId= [Select Apttus_Proposal__Opportunity__c                                            
                                      from Apttus_Proposal__Proposal__c
                                      Where id =:proposalId];
        String oppID1 = oppId[0].Apttus_Proposal__Opportunity__c;
        for(Lot__c lot : [Select ID , Name                                           
                                      from Lot__c
                                      Where name in :setLot and Opportunity__c =:oppID1]){
             for(Sub_Lot__c subLot : [Select ID , Name                                           
                                      from Sub_Lot__c
                                      Where name in :setSubLot and Lot__c=: lot.id]){
              for(Customer_Product_Description__c custProd : [Select ID , Name                                           
                                      								from Customer_Product_Description__c
                                      								Where name in :setCustProd and Sub_Lot__c=: subLot.id]){
           mapCustProd.put(custProd.name,custProd);
        }
                             
            mapSubLot.put(subLot.name,subLot);
        }                             
                                          
            mapLot.put(lot.name,lot);
        }
        
        for(ProductWrapperClass invalidProduct : listInvalidProducts){
            invalidProduct.isSelected = false;
            if(invalidProduct.lot != ''){
                
                if((mapProduct.containsKey(invalidProduct.productCode))
                &&(mapLot.containsKey(invalidProduct.lot))
               		&&(mapSubLot.containsKey(invalidProduct.subLot))
               			&&(mapCustProd.containsKey(invalidProduct.custProd))){
               
                Product2 productSO = mapProduct.get(invalidProduct.productCode);
                Lot__c lot1 = mapLot.get(invalidProduct.lot);
                Sub_Lot__c subLot1 = mapSubLot.get(invalidProduct.subLot);
                Customer_Product_Description__c cProduct = mapCustProd.get(invalidProduct.custProd);
                
                invalidProduct.productId = productSO.ID;
                invalidProduct.name = productSO.name; 
                invalidProduct.lotID  =  lot1.ID;
                invalidProduct.subLotID  =  subLot1.ID;
                invalidProduct.custProdID = cProduct.ID;
                
                if(invalidProduct.quantity > 0){
                    invalidProduct.error = '';
                    listValidProducts.add(invalidProduct);
                    invalidProduct.isSelected = true;
                }
                else{
                    invalidProduct.error = 'Invalid Quantity';
                }
            }
            else{
                if(!(mapProduct.containsKey(invalidProduct.productCode))){
                    invalidProduct.error = 'Please Check Product Code';
                }else if(!(mapLot.containsKey(invalidProduct.lot))){
                    invalidProduct.error = 'Please Check Lot Name';
                }else if(!(mapSubLot.containsKey(invalidProduct.subLot))){
                    invalidProduct.error = 'Please Check Sub Lot Name';
                }else{
                    invalidProduct.error = 'Please Check Customer Ref# ';
                }
            }}
                else{
                    invalidProduct.subLot = '';
                    invalidProduct.custProd = '';
                    if(mapProduct.containsKey(invalidProduct.productCode)){
                Product2 productSO = mapProduct.get(invalidProduct.productCode);
                invalidProduct.productId = productSO.ID;
                invalidProduct.name = productSO.name;                    
                if(invalidProduct.quantity > 0){
                    invalidProduct.error = '';
                    listValidProducts.add(invalidProduct);
                    invalidProduct.isSelected = true;
                }
                else{
                    invalidProduct.error = 'Invalid Quantity';
                }
            }
            else{
                invalidProduct.error = 'Invalid Product Code';
            }
                }
            }
            
        
        
        list<ProductWrapperClass> listUpdatedInvalidProducts = new list<ProductWrapperClass>();
        for(ProductWrapperClass invalidProduct : listInvalidProducts){
            if(!invalidProduct.isSelected){
                listUpdatedInvalidProducts.add(invalidProduct);
            }
        }
        listInvalidProducts.clear();
        listInvalidProducts.addAll(listUpdatedInvalidProducts);
    }
    
    @RemoteAction
    public static Boolean addMultiProductsVF(String cartID, String JSONlistValidProducts,String strStartIndex, String strEndIndex){
        
        Savepoint sp = Database.setSavepoint();
        Integer startIndex = Integer.valueof(strStartIndex);
        Integer endIndex = Integer.valueof(strEndIndex);
        List<ProductWrapperClass> listValidProducts = (List<ProductWrapperClass>)JSON.deserialize(JSONlistValidProducts,List<APTS_UploadCSVController.ProductWrapperClass>.class);
        List<Apttus_CPQApi.CPQ.SelectedProductDO> selectedProdDOList = new List <Apttus_CPQApi.CPQ.SelectedProductDO>();
        
        Integer listsize = 0;
        if(listValidProducts != null){
            listsize = listValidProducts.size();
        }
        if(startIndex >= listsize){
            return true;
        }
        
        for(Integer i=startIndex ; i<endIndex && i<listsize ; i++) {
            ProductWrapperClass selProdWrap = listValidProducts[i];
            Apttus_CPQApi.CPQ.SelectedProductDO selProdDO = new Apttus_CPQApi.CPQ.SelectedProductDO();
            selProdDO.ProductID = selProdWrap.ProductId;
            selProdDO.Quantity = selProdWrap.Quantity;
            //Setting Custom Fields
            List<String> customFields = new List<String>();  
            customFields.add('Lot__c');         
            customFields.add('Sub_Lot__c');
            customFields.add('Customer_Ref_Nb__c');
            selProdDO.CustomFields = customFields;
 			//Setting Custom Data
            Apttus_Config2__LineItem__c liSO = new Apttus_Config2__LineItem__c();       
            liSO.Lot__c = selProdWrap.lotID;
            liSO.Sub_Lot__c = selProdWrap.subLotID;
            liSO.Customer_Ref_Nb__c = selProdWrap.custProdID;
          	selProdDO.CustomData = liSO;
            
            selectedProdDOList.add(selProdDO);
        }
        
        if(selectedProdDOList.size()>0){
            Apttus_CPQApi.CPQ.AddMultiProductRequestDO request = new Apttus_CPQApi.CPQ.AddMultiProductRequestDO ();
            System.debug('Cart Id In add Multi: '+cartId);
            request.CartId = cartId;
            request.SelectedProducts = selectedProdDOList;
            try{
                Apttus_CPQApi.CPQ.AddMultiProductResponseDO response = Apttus_CPQApi.CPQWebService.addMultiProducts(request);            
                if(response.LineNumbers != null && response.LineNumbers.size() == selectedProdDOList.size()){
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Confirm,'Products are Imported Successfully'));
                    return true;                    
                }
                else{
                    Database.rollback(sp);
                    }
            }
            catch(Exception exceptionSO){
                Database.rollback(sp);
            }
        }
        return false;
    }
    
    @RemoteAction
    public static Boolean saveCart(String cartID){
        System.debug('Inside Save Cart '+ cartID);
        List<Apttus_Config2__ProductConfiguration__c> listCart = [Select ID
                                                                		, Apttus_Config2__Status__c
                                                                		from Apttus_Config2__ProductConfiguration__c
                                                                		Where ID = :cartId];
        if(listCart != null
           && !listCart.isEmpty()){
           listCart[0].Apttus_Config2__Status__c = 'Saved';
           update listCart[0];
           return true;
        }
        return false;
    }
    
    public class ProductWrapperClass{
        public String productId {get; set;}
        public Integer index {get; set;}
        public String name {get; set;}
        public String productCode {get; set;}
        public Integer quantity {get; set;}
        public Boolean isSelected {get; set;}
        public String error {get; set;}
        public String lot {get; set;}
        public String subLot {get; set;}
        public String custProd {get; set;}
        public ID lotID {get; set;}
        public ID subLotID {get; set;}
        public ID custProdID {get; set;}
        
        public ProductWrapperClass(Integer index1, String productCode1, String stringQuantity, String lot1, String subLot1,String custProd1){
            index = index1;
            productCode = productCode1;
            quantity = 0;
            if(stringQuantity != null && stringQuantity.isNumeric()){
                quantity    = Integer.valueOf('0'+stringQuantity);
            } 
            productId   = '';
            isSelected  = false;
            error = '';
            lot = lot1;
            subLot = subLot1;
            custProd = custProd1;
            lotID = null;
            subLotID = null;
            custProdID = null;
        }
    }
}