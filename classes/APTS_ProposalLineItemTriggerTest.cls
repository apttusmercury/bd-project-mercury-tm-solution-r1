@isTest
public class APTS_ProposalLineItemTriggerTest {
    
    static testMethod void VataRateTest(){
        Test.startTest();
    
        ID Rid = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Temporary Account').getRecordTypeId();
        
        ID Rid1 = Schema.SObjectType.Account.getRecordTypeInfosByName().get('Competitor').getRecordTypeId();
        TriggerFactory.bypassApex=true;
       
        Account CA =new Account(Name='TestAccount1',RecordTypeId=Rid1,BillingCountry='Belgium',BillingStreet='abc',BillingCity='xyz',AccountCategory__c='Government',IsTopLevelAccount__c=true);
        insert CA;
        
        Account acc=new Account(Name='TestAccount2',RecordTypeId=Rid,BillingCountry='Belgium',BillingStreet='abc',BillingCity='xyz',AccountCategory__c='Government',IsTopLevelAccount__c=true);
        insert acc;
        
        Account accComp=new Account(ParentID=acc.ID,Name='TestAccount1',BillingCountry='Belgium',BillingStreet='abc',BillingCity='xyz',RecordTypeId=Rid);
        insert accComp;
        
        Opportunity opp = new Opportunity(name='TestOpp',AccountID=acc.Id,DealType__c='Standard BD Offer',CloseDate=System.today()+30,StageName='Identification',Competitor__c=CA.Id);
        insert opp;
           
        Apttus_Proposal__Proposal__c p = new Apttus_Proposal__Proposal__c(Apttus_Proposal__Account__c=acc.ID,Apttus_Proposal__Opportunity__c=opp.Id,Language__c='Danish');
		insert p;
     
        Country__c c = new Country__C(name='Belgium');
        insert c;
     
        Language__c l = new Language__c(name='Danish');
        insert l;
    
        Product2 p1 = new Product2(Name='TestProduct',CatalogNumber__c='1111');
        insert p1;
        
        Apttus_Proposal__Proposal_Line_Item__c pli = new Apttus_Proposal__Proposal_Line_Item__c(Apttus_Proposal__Proposal__c=p.ID,Apttus_Proposal__Product__c=p1.ID,DescriptionInLocalLanguage__c='abc');
        insert pli;
        
        Apttus_Proposal__Proposal_Line_Item__c prop1=new Apttus_Proposal__Proposal_Line_Item__c(Apttus_Proposal__Proposal__c=p.Id,Apttus_QPConfig__NetPrice__c=-10.5);
	    insert prop1;
   
        ProductLanguageDesc__c pld=new  ProductLanguageDesc__c(Language__c=l.ID,ShortDescription__c='abc',CatalogNumber__c=p1.ID);
        insert pld;
     
        Product_Country__c pCountry = new Product_Country__c(CatalogNumber__c=p1.ID,Country__c=c.ID,TaxRate__c=2.5);
        insert pCountry;
       
        pli.Apttus_Proposal__Description__c ='update Test';
        update pli;
      
        Test.stopTest();
    }
}