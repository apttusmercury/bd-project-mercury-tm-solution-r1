@isTest
public class APTS_TaskTriggerTest {
    
     static testMethod void taskTest(){
         Test.startTest();
         Task task = new Task(Subject='Create dossier',Description='dossier');
         insert task;
         Task task1 = new Task(Subject='Timelines and deadlines',Description='Timelines and deadlines');
         insert task1;
         Task task2 = new Task(Subject='Identify BU(s)',Description='Test');
         insert task2;
         Task task3 = new Task(Subject='Identify customer(s)',Description='Test');
         insert task3;
         Task task4 = new Task(Subject='Download documentation',Description='Test');
         insert task4;
         Task task5 = new Task(Subject='Book start up meeting',Description='Test');
         insert task5;
         Task task6 = new Task(Subject='Create checklist of tasks',Description='Test');
         insert task6;
         Task task7 = new Task(Subject='Feasibility assessment',Description='Test');
         insert task7;
         Task task8 = new Task(Subject='Pre tender documentation',Description='Test');
         insert task8;
         
         
         Test.stopTest();
     }


}